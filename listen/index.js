import { Server } from "node-osc";

const ip = "0.0.0.0";
const port = 3333;
const BEGIN_MARKER = "~";
const END_MARKER = "!";
const b64Alphabet = `ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=${BEGIN_MARKER}${END_MARKER}`;
const BEGIN = b64Alphabet.split("").indexOf(BEGIN_MARKER);
const END = b64Alphabet.split("").indexOf(END_MARKER);

const oscServer = new Server(port, ip, () => {
  console.log(`Listening: ${ip}:${port}`);
});

let agg;
oscServer.on("message", function (msg) {
  const incoming = b64Alphabet.split("")[msg[1]];
  console.log(msg);
  switch (incoming) {
    case BEGIN_MARKER:
      agg = [];
      break;
    case END_MARKER:
      const b64 = agg.join("");
      const data = Buffer.from(b64, "base64").toString("utf8");
      console.log(JSON.parse(data));

      break;
    default:
      agg.push(incoming);
      break;
  }
});
