import { Client, Server } from "node-osc";
import fetch from "node-fetch";
import js2xmlparser from "js2xmlparser";

const config = {
  xmlOutput: false,
  tx: {
    ip: "127.0.0.1",
    port: 9000
  },
  rx: {
    ip: "0.0.0.0",
    port: 9001
  }
};

const apiUrl = "https://random-data-api.com/api/address/random_address";
const BEGIN_MARKER = "~";
const END_MARKER = "!";
const b64Alphabet = `ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=${BEGIN_MARKER}${END_MARKER}`;
const BEGIN = b64Alphabet.split("").indexOf(BEGIN_MARKER);
const END = b64Alphabet.split("").indexOf(END_MARKER);

let LAST_SENT = null;
let LAST_ACK = null;
let ACK_VAL = null;
let TX_START;
let MSG_POINTER;
let OUTGOING_DATA;
let IS_SENDING = false;
let RESEND_TIMER = null;

const listen = new Server(config.rx.port, config.rx.ip, () => {
  console.log(`LISTENING: ${config.rx.ip} ${config.rx.port}`);

  //-------------------------------------------------------------------------------------------

  console.log(`Fetching data from: ${apiUrl}...`);
  fetch(apiUrl)
    .then((res) => res.json())
    .then((json) => {
      OUTGOING_DATA = config.xmlOutput
        ? js2xmlparser.parse("data", json)
        : JSON.stringify(json);

      console.log("------------------------------------------------------");
      console.log(OUTGOING_DATA);
      console.log("------------------------------------------------------");
      send(0, () => {
        TX_START = new Date();
        MSG_POINTER = 0;
        IS_SENDING = true;
        const b64Data = Buffer.from(OUTGOING_DATA).toString("base64");
        send(b64Alphabet.split("").indexOf(b64Data.split("")[MSG_POINTER]));
      });
    });
});

listen.on("message", function (msg) {
  const address = msg[0];
  if (address.includes("data_pipe_01") && IS_SENDING) {
    clearTimeout(RESEND_TIMER);
    LAST_ACK = new Date();
    ACK_VAL = msg[1];
    const b64Data = Buffer.from(OUTGOING_DATA).toString("base64");
    if (ACK_VAL === LAST_SENT) {
      txNext();
    } else {
      clearTimeout(RESEND_TIMER);
      console.log(`\nACK FAIL: Expected ${LAST_SENT} got ${ACK_VAL}`);
      send(b64Alphabet.split("").indexOf(b64Data.split("")[MSG_POINTER]));
    }
  }
});

const txNext = () => {
  const b64Data = Buffer.from(OUTGOING_DATA).toString("base64");
  MSG_POINTER++;
  printProgress(
    `Sending: ${Math.round((MSG_POINTER / b64Data.split("").length) * 100)}%`
  );
  send(b64Alphabet.split("").indexOf(b64Data.split("")[MSG_POINTER]));
  if (MSG_POINTER === b64Data.split("").length) {
    IS_SENDING = false;
    send(0);
    console.log("\n------------------------------------------------------");
    console.log(`\nCompleted in ${(new Date() - TX_START) / 1000} seconds`);
  }
};

function printProgress(progress) {
  process.stdout.clearLine();
  process.stdout.cursorTo(0);
  process.stdout.write(progress);
}

const send = (data, cb) => {
  if (RESEND_TIMER) clearTimeout(RESEND_TIMER);
  RESEND_TIMER = setTimeout(() => {
    clearTimeout(RESEND_TIMER);
    if (IS_SENDING) {
      if (data === ACK_VAL) {
        console.log(`\nWARN: Repeat, assuming ok ${data}`);
        txNext();
      } else {
        console.log(`\nRESENDING ${data}`);
        send(data, cb);
      }
    }
  }, 100);

  setTimeout(() => {
    const client = new Client(config.tx.ip, config.tx.port);
    client.send("/avatar/parameters/data_pipe_01", data, () => {
      LAST_SENT = data;
      client.close();
      if (typeof cb === "function") cb();
    });
  }, 10);
};
